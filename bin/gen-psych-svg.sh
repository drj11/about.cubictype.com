#!/bin/sh

set -e

hb-view --line-space=-60 --background ff0099 --foreground ffff66 --output-file=psych-swift.svg static/TTF/VICTM___.TTF 'Psyche
delia
Swift'
