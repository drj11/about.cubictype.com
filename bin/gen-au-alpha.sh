#!/bin/sh

hb-view --background 22aa44 --foreground ffffff --line-space -60 --output-file au.svg "$@" static/OTF/auncial-Regular.otf 'abcde
fghijkl
mnopq
rstuv
wxyzþ'
