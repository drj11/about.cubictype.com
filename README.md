# This is the about page for the Cubic Type studio

Cubic Type is a design studio and font foundry.

## Cubic Type blurb


Description for myfonts.com &c.
Requirements:
- myfonts.com; must be at least 350 characters (not counting
  spaces).

Cubic Type is committed to social justice, equality, and
excellence through competence.
Every aspect of our work contributes to the message;
no detail is considered too small or trivial.
Everything gets designed.

Cubic Type specialises in high-quality kerning,
data-driven research to font-design decisions, and
programmed OpenType features.
Cubic Type has a particular interest in making sure that
minority languages and scripts are not left to wither and die
from lack of support in modern typography.

Cubic Type has been operating since 2020 from Sheffield, UK.


## David Jones designer bio

Must be at least 200 characters for myfonts.com

